"""
Copyright 2021 NoRD Software Foundation

This file is part of NoRD.

NoRD is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

NoRD is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with NoRD.  If not, see <https://www.gnu.org/licenses/>.

--

"""
import nord.nord.Runtime as RT
import nord.nord.Map as Map
import json


Runtime = RT.Runtime


def prep_for_run(comparer, v1, v2):
    try:
        m = json.load(open('tests/maps/test_all_comparers.n', 'r'))
    except FileNotFoundError:
        m = json.load(open('kamal/tests/maps/test_all_comparers.n', 'r'))

    for node in m["map"]["nodes"]:
        if node["id"] == "comparer":
            node["type"] = comparer
            break

    karte = Map(m, verbose=False)

    a = karte.get_node("a")
    b = karte.get_node("b")

    a.value = v1
    b.value = v2

    vessal = Runtime()
    karte.run(vessal, None)
    assert vessal.get_anomaly() is None
    return vessal, karte
