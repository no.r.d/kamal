"""
Copyright 2019 NoRD Software Foundation

This file is part of NoRD.

NoRD is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

NoRD is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with NoRD.  If not, see <https://www.gnu.org/licenses/>.

--

"""

import nord.kamal.nodes.Comparer as Comparer
import nord.nord.edges.Flow as Flow
import nord.nord.edges.Pass as Pass
import nord.nord.nodes.Data as Data
from nord.nord.exceptions import RuntimeException as RuntimeException


def test_comparer():
    c = Comparer(None)
    d = Data(None)
    f = Flow(None)
    f.set_nodes(d, c, 'flow')

    try:
        c.validate()
    except RuntimeException as e:
        assert str(e) == 'No values passed to comparer: Comparer'

    try:
        c.compare(1, 2)
    except RuntimeException as e:
        assert str(e) == 'Base Comparer method called'

    p = Pass(None)
    p.set_nodes(d, c, 'pass')
    try:
        c.validate()
    except RuntimeException as e:
        assert str(e) == 'Not enough values passed to comparer: Comparer'
