{
    "extensions": {
        "athena": {
            "locations": {
                "code": [
                    "null://."
                ],
                "files": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/athena/src/nord/athena",
                    "pkg://"
                ],
                "path": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/athena/src/nord/athena",
                    "pkg://"
                ]
            }
        },
        "enrique": {
            "locations": {
                "code": [
                    "null://."
                ],
                "files": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/enrique/src/nord/enrique",
                    "pkg://"
                ],
                "path": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/enrique/src/nord/enrique",
                    "pkg://"
                ]
            }
        },
        "fregata": {
            "locations": {
                "code": [
                    "null://."
                ],
                "files": [
                    "dir://kamal/tests/maps",
                    "dir:///Users/nate/Desktop/Dev/NoRD/fregata/src/nord/fregata",
                    "pkg://"
                ],
                "path": [
                    "dir://kamal/tests/maps",
                    "dir:///Users/nate/Desktop/Dev/NoRD/fregata/src/nord/fregata",
                    "pkg://"
                ]
            }
        },
        "iteru": {
            "locations": {
                "code": [
                    "null://."
                ],
                "files": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/iteru/src/nord/iteru",
                    "pkg://"
                ],
                "path": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/iteru/src/nord/iteru",
                    "pkg://"
                ]
            }
        },
        "jabir": {
            "locations": {
                "code": [
                    "null://."
                ],
                "files": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/jabir/src/nord/jabir",
                    "pkg://"
                ],
                "path": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/jabir/src/nord/jabir",
                    "pkg://"
                ]
            }
        },
        "kamal": {
            "locations": {
                "code": [
                    "null://."
                ],
                "files": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/kamal/src/nord/kamal",
                    "pkg://"
                ],
                "path": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/kamal/src/nord/kamal",
                    "pkg://"
                ]
            }
        },
        "pasion": {
            "locations": {
                "code": [
                    "null://."
                ],
                "files": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/pasion/src/nord/pasion",
                    "pkg://"
                ],
                "path": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/pasion/src/nord/pasion",
                    "pkg://"
                ]
            }
        },
        "shoshoni": {
            "locations": {
                "code": [
                    "null://."
                ],
                "files": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/shoshoni/src/nord/shoshoni",
                    "pkg://"
                ],
                "path": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/shoshoni/src/nord/shoshoni",
                    "pkg://"
                ]
            }
        },
        "sushrut": {
            "locations": {
                "code": [
                    "null://."
                ],
                "files": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/sushrut/src/nord/sushrut",
                    "pkg://"
                ],
                "path": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/sushrut/src/nord/sushrut",
                    "pkg://"
                ]
            }
        },
        "wrigan": {
            "locations": {
                "code": [
                    "null://."
                ],
                "files": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/wrigan/src/nord/wrigan",
                    "pkg://"
                ],
                "path": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/wrigan/src/nord/wrigan",
                    "pkg://"
                ]
            }
        },
        "xuyue": {
            "locations": {
                "code": [
                    "null://."
                ],
                "files": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/xuyue/src/nord/xuyue",
                    "pkg://"
                ],
                "path": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/xuyue/src/nord/xuyue",
                    "pkg://"
                ]
            }
        }
    },
    "map": {
        "edges": [
            {
                "rule": "contains",
                "source": "2bea0c7f-dc75-4d51-b2a9-d33c115f7892:main",
                "target": "80a2d1e5-116e-452c-8113-b527bb332f89:Pass"
            },
            {
                "from_space": "nord",
                "rule": "contains",
                "source": "2bea0c7f-dc75-4d51-b2a9-d33c115f7892:main",
                "space": "nord",
                "target": "comparer:Comparer",
                "to_space": "kamal"
            },
            {
                "rule": "contains",
                "source": "2bea0c7f-dc75-4d51-b2a9-d33c115f7892:main",
                "target": "9d24a74f-563b-4c18-9495-b613d162d7cb:pass output"
            },
            {
                "rule": "contains",
                "source": "2bea0c7f-dc75-4d51-b2a9-d33c115f7892:main",
                "target": "b:b"
            },
            {
                "rule": "contains",
                "source": "2bea0c7f-dc75-4d51-b2a9-d33c115f7892:main",
                "target": "a:a"
            },
            {
                "rule": "contains",
                "source": "2bea0c7f-dc75-4d51-b2a9-d33c115f7892:main",
                "target": "ec1ea263-45bd-4ee3-9576-ebe17e17b6ef:fail output"
            },
            {
                "rule": "contains",
                "source": "2bea0c7f-dc75-4d51-b2a9-d33c115f7892:main",
                "target": "f8b2021b-c0e9-4be3-ad41-a30e21235a0a:Fail"
            },
            {
                "rule": "pass",
                "source": "80a2d1e5-116e-452c-8113-b527bb332f89:Pass",
                "target": "9d24a74f-563b-4c18-9495-b613d162d7cb:pass output"
            },
            {
                "from_space": "kamal",
                "rule": "trueflow",
                "source": "comparer:Comparer",
                "space": "kamal",
                "target": "80a2d1e5-116e-452c-8113-b527bb332f89:Pass",
                "to_space": "nord"
            },
            {
                "from_space": "kamal",
                "rule": "falseflow",
                "source": "comparer:Comparer",
                "space": "kamal",
                "target": "f8b2021b-c0e9-4be3-ad41-a30e21235a0a:Fail",
                "to_space": "nord"
            },
            {
                "from_space": "nord",
                "rule": "pass",
                "source": "b:b",
                "space": "nord",
                "target": "comparer:Comparer",
                "to_space": "kamal"
            },
            {
                "from_space": "nord",
                "rule": "pass",
                "source": "a:a",
                "space": "nord",
                "target": "comparer:Comparer",
                "to_space": "kamal"
            },
            {
                "rule": "pass",
                "source": "f8b2021b-c0e9-4be3-ad41-a30e21235a0a:Fail",
                "target": "ec1ea263-45bd-4ee3-9576-ebe17e17b6ef:fail output"
            }
        ],
        "nodes": [
            {
                "id": "2bea0c7f-dc75-4d51-b2a9-d33c115f7892",
                "name": "main",
                "position": [
                    -3.9502997398376465,
                    0.805023193359375,
                    1.3414390087127686
                ],
                "space": "nord",
                "type": "container"
            },
            {
                "id": "80a2d1e5-116e-452c-8113-b527bb332f89",
                "name": "Pass",
                "position": [
                    -5.016493320465088,
                    0.07546615600585938,
                    -3.6853480339050293
                ],
                "space": "nord",
                "type": "static",
                "value": "Pass"
            },
            {
                "id": "comparer",
                "name": "Comparer",
                "position": [
                    -0.18758797645568848,
                    0.22130584716796875,
                    0.4007514715194702
                ],
                "space": "kamal",
                "type": "equal"
            },
            {
                "id": "9d24a74f-563b-4c18-9495-b613d162d7cb",
                "name": "pass output",
                "position": [
                    -4.949141502380371,
                    -0.8019256591796875,
                    -9.05936336517334
                ],
                "space": "nord",
                "type": "builtin"
            },
            {
                "id": "b",
                "name": "b",
                "position": [
                    6.066741943359375,
                    -0.5591583251953125,
                    5.214017391204834
                ],
                "space": "nord",
                "type": "static",
                "value": "b"
            },
            {
                "id": "a",
                "name": "a",
                "position": [
                    -5.2021965980529785,
                    -0.6508712768554688,
                    5.450394153594971
                ],
                "space": "nord",
                "type": "static",
                "value": "a"
            },
            {
                "id": "ec1ea263-45bd-4ee3-9576-ebe17e17b6ef",
                "name": "fail output",
                "position": [
                    6.363754749298096,
                    0.9364242553710938,
                    -9.68654727935791
                ],
                "space": "nord",
                "type": "builtin"
            },
            {
                "id": "f8b2021b-c0e9-4be3-ad41-a30e21235a0a",
                "name": "Fail",
                "position": [
                    6.349216938018799,
                    0.2607536315917969,
                    -3.926180362701416
                ],
                "space": "nord",
                "type": "static",
                "value": "Fail"
            }
        ]
    },
    "name": "Test Kamal Comparer"
}