"""
Copyright 2019 NoRD Software Foundation

This file is part of NoRD.

NoRD is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

NoRD is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with NoRD.  If not, see <https://www.gnu.org/licenses/>.

--
Facilitate Containing this type of comparison.
"""

import nord.nord.Rule as Rule
import sys


class ContainsGreaterequal(Rule):  # pragma: no cover
    """The ContainsGreaterEqual rule allows the related target in the container."""

    def apply(self):
        """Implement the apply method."""
        return True


sys.modules[__name__] = ContainsGreaterequal
