"""
Copyright 2019 NoRD Software Foundation

This file is part of NoRD.

NoRD is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

NoRD is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with NoRD.  If not, see <https://www.gnu.org/licenses/>.

--
Hidden base module implementing all logic foundational methods.
"""

import nord.nord.Node as Node
from nord.nord.exceptions import RuntimeException as RuntimeException
import sys


class Comparer(Node):
    """Base object that provides common methods to all children."""

    def compare(self, a, b):
        """Placeholder method to be overridden by children."""
        raise RuntimeException(_("Base Comparer method called"))  # noqa: F821

    def validate(self):
        """Ensure this node can be run."""
        if 'pass' not in self._sourceRules:
            raise RuntimeException(_("No values passed to comparer: {}")  # noqa: F821
                                   .format(self.__class__.__name__))
        if len(self._sourceRules['pass']) < 2:
            raise RuntimeException(_("Not enough values passed to comparer: {}")  # noqa: F821
                                   .format(self.__class__.__name__))

    def vis_repr(self):
        if hasattr(self, 'vis_exe_repr'):
            return self.vis_exe_repr
        else:
            return self._str_equiv.join([str(x.get_source().__class__.__name__) for x in self.get_sources('pass')])

    def execute(self, runtime):
        """
        Call the child methods to establish flow along appropriate edge.

        validate there are at least 2 items passed to this node
        call the child comparison function on all passed cargo's
        in order.
        """
        self.validate()
        # Make sure this node's true&false outbound edges are
        # able to be follewed.
        # super().reset_target_edges()
        values = list()
        for edge in sorted(self.get_sources('pass')):
            if edge.has_cargo():
                values.append(edge.get_cargo())
            else:
                raise RuntimeException(_("Comparer {} has empty edge: {}")  # noqa: F821
                                       .format(self.__class__.__name__, edge))

        # By setting this variable whose name does not start with a '_'
        # we are taking advantage of the CDC system on the node
        # to synchronize this value with the subscribers. This probably
        # should only be done when the map is being debugged...
        if runtime.debug:
            self.vis_exe_repr = self._str_equiv.join([str(x) for x in values])

        passed = True
        for a, b in zip(values[:-1], values[1:]):
            # print ( "Comparing {} to {}".format(a,b))
            if not self.compare(a, b):
                passed = False
                break

        if passed:
            flag_followed = 'falseflow'
            flow_convert = 'trueflow'
        else:
            flag_followed = 'trueflow'
            flow_convert = 'falseflow'

        for edge in self.get_targets(flag_followed):
            edge.flag_followed(runtime)
            # Mark the target as executed
            edge.get_target().flag_exe(runtime)
            # Make sure any edges out ef the target
            # are marked as followed as well.
            edge.get_target().flag_followed(runtime)
            # edge.get_target().next_after_execute(runtime, runtime.verbose, affect_schedule=True)

        self.flag_exe(runtime)

        for edge in self.get_targets(flow_convert):
            edge.clear_followed()
            # e = Flow(self.get_graph())
            # e._from_compare = True
            # e.set_nodes(edge.get_source(), edge.get_target(), 'flow')
            # edge.follow(runtime)
            pass
            # edge.get_target().reset_target_edges(recurse=True)
            # edge.get_target().next_after_execute(runtime, runtime.verbose, affect_schedule=True)

        # The flow edges should increment the exec_counter, not the node
        # runtime.incr_exec_count()
        # reset source edges to unfollowed, so they will be re-run
        # self.reset_source_edges()
        pass

    # def next_after_execute(self, runtime, verbose):
    #     """
    #     Remove residual flow edges created during execution.

    #
    #     This appears to be Deprecated.... at least the loop at the end
    #     of this function
    #     """
        # if len(self._unfollowedSources) == 0:
        #     self.flag_exe(runtime)
        # super().next_after_execute(runtime, verbose)
        # for edge in self.get_targets('flow'):
        #     if hasattr(edge, '_from_compare'):
        #         self.rem_target(edge.get_target(), edge)
        #         self.get_graph().rem_edge(edge)

    # def reset_target_edges(self, recurse=False, clear_exe=False):
    #     """
    #     Override the Node's reset_target edges,
    #     as the comparer will manage their called-ness
    #     on its own.
    #     """
    #     pass


sys.modules[__name__] = Comparer
