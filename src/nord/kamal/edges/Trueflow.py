"""
Copyright 2019 NoRD Software Foundation

This file is part of NoRD.

NoRD is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

NoRD is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with NoRD.  If not, see <https://www.gnu.org/licenses/>.

--
Edge definition to enable comparison outcomes.
"""

import nord.nord.edges.Flow as Flow
import sys


class Trueflow(Flow):
    """Override the active methods as Comparer does the work."""

    def traverse(self, srule, trule):  # pragma: no cover
        """Do nothing at all. Not called, as follow is owerridden"""
        return True

    def follow(self, runtime, verbose=False):
        """Override the edge follow to do nothing at all."""
        super().follow(runtime, verbose)
        self.flag_followed(runtime)


sys.modules[__name__] = Trueflow
